---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

Key:

- 👁️ This project gives you insight into something in my apartment
- 🎤 This project lets me communicate with you
- 🚨 This project will turn on/off/change lights in my apartment
- 🔊 This project will play a sound (or your voice) in my apartment
- 🖨️ This project lets you print/display something in my apartment

## Physical-digital integration

I have this interest in connecting the physical and digital worlds, and a few of my projects work towards that goal.

### Wallflower 🚨

[https://wallflower.ericbetts.dev/](https://wallflower.ericbetts.dev/)
I have Nanoleaf light panels (aka "aurora") on my living room wall. You can control the color!
It will also show a toast message and update if someone else has the page open and makes a change. If the page loads with grey panels, give it a few seconds or try reloading.

### Joy 👁️

[https://joy.ericbetts.dev/](https://joy.ericbetts.dev/)

Using machine learning and a camera pointed at where I stand while using the computer at home or work, see my joy score. More details on the page.

### Meeseeks Box 🔊

[https://meeseeks-box.ericbetts.dev/](https://meeseeks-box.ericbetts.dev/)

Named for a character on "Rick & Morty"; A Raspberry Pi Zero with a speaker and a button will play a random clip of the character when you hit the button. The website acts as a virtual button: press it and the speaker in my living room will play a clip just like if you hit the real, physical, button.

[meeseeks.mov](meeseeks.mov)

### Talkiepi (intercom) 🔊 🎤

Originally designed for inter-team communication, this is another Raspberry Pi Zero with a speaker, microphone and button. Using [mumble](https://www.mumble.info/downloads/) (open source VoIP) if you connect to `[shouldigotochipotle.com:64738](http://shouldigotochipotle.com:64738)` (random public server I chose when I started the project 2 years ago), and join "Room 24", you'll see an 'intercom-bettse' user. That is my intercom. Anything you say in the room will be played through the speaker on my desk, and I can reply by holding down the button and speaking.

### Deploy Receipt 🖨️

[https://deploy-receipt.ericbetts.dev/](https://deploy-receipt.ericbetts.dev/)

Prints a receipt for each deploy. See page for exact URL and example receipt.

### Swipe Deploy

[https://swipe-deploy.netlify.app/](https://swipe-deploy.netlify.app/)

[Swipe_Deploy.mp4](Swipe_Deploy.mp4)

I encoded a site id and deploy id on a magstripe. I am using a magstripe reader that emulates a keyboard, and is submitting via an invisible, but focused, input field to a netlify function. The function calls the netlify api to update the site's active deploy and redirect to the site.
To illustrate, I have deploys use different css themes. Because of Netlify's instant deploys, the new css is shown by the time we redirect to it. In the future, Oauth could be added to support sites beyond my personal account.

### Badges of / Badge me 🖨️

[https://badgesof.ericbetts.dev/](https://badgesof.ericbetts.dev/)

I have an Evolis Primacy badge/id card printer that I put online and semi-automated. You can submit an address and an image (using Netlify Forms 🎉), the image will be printed on a badge (specifically a PVC CR80), and the address, my return address, and a copy of the badge will be printed using a normal printer. I folder up the badge in the paper, drop it in a windowed envelope, stamp and mail it (including international).

(The print server can be a little fickle, so please ping me if it says it is offline)

### Lux Populi Omnibus (incomplete) 🖨️

[https://luxpopuliomnibus.ericbetts.dev/](https://luxpopuliomnibus.ericbetts.dev/)

When Matt/Chris present and share their screen with the whole company, all the participant videos become small and many are hidden. This makes it hard for them to see the audience and gauge reaction. I wanted to solve this by sending them each a WiFi-enabled LED panel, each light controlled by a Netlifier, so that we could create waves of color and provide feedback during presentations.

While I was able to pin down the technicals (the website, the panels, etc), making it into something beyond a proof of concept provided problematic. Scaling the backend to support the large number of updates that would happen during a presentation was difficult without incurring non-trivial costs. The panels themselves would need an easy way to set them up with WiFi once they arrived to Matt/Chris, and there are a variety of methods for that, but each has some significant trade-offs.

## Other projects

### Maplify

[https://maplify.netlify.app/](https://maplify.netlify.app/)

Pulling data from BambooHR, this maps the city (US only), state, country of my fellow Netlifiers. Helping to get a better sense of where someone is both spatially and temporally. Based on feedback during my initial presentation of it, the map tiles are a water color design that is more aesthetic and less precise.

Contact PeopleOps if you'd like to reduce the information available to your coworkers in BambooHR

### Deploy timeline

[https://deploy-timeline.netlify.app/](https://deploy-timeline.netlify.app/)

Netlfy takes a screenshot of each deploy, and I thought this was a great way to see how a site evolved over time. Using this, you can get a slideshow of your deploy screenshots, as well as a gif.

### I use a mac

[https://iuseamac.com/](https://iuseamac.com/)

I kept using the phrase "I use a mac" while on a video call with a social group, and they started teasing that I needed that as a website (and that it was available). So obviously I _had_ to buy the domain (through Netlify) and put up a site (hosted with Netlify).

### Family emoji maker

[https://familyemojimaker.netlify.app/](https://familyemojimaker.netlify.app/)

Not everyone in my family shares the same skin tone, but when it comes to emoji, the supported combinations aren't always available. Using this tool, you can select parents and children to see the combined emoji (when it exists). A longer description with examples is on the page.

### Sockethook

[https://sockethook.ericbetts.dev/](https://sockethook.ericbetts.dev/)

This is for the technical crowd. I like websockets, and while researching how we might use them for our own site ([https://app.netlify.com/](https://app.netlify.com/)), I came up with an idea: There are lots of places that do webhooks (including Netlify!), what if you could get that data in a websocket and act on it? You could reload a deploy preview when the PR changed, reload a website when a new production deploy happened, etc. I found a [project](https://github.com/fabianlindfors/sockethook) that did this, but required deploying it yourself. I wanted to make something I could use, and could share with others who wanted to try it.

The page contains details on how to use it, and a little demo that acts a bit like an on-page chat.

### Betts of Netlify

[https://bettsofnetlify.com/](https://bettsofnetlify.com/)

My bad pun on, and homage to, [https://petsof.netlify.com/](https://petsof.netlify.com/).

I just ordered a collection of fake mustaches and soon I hope to add a slideshow to the page.

**June 7th, 2021** I've updated the page with a bunch of photos, and still hope to add more with fake mustaches.
